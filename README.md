# cannal-es

#### 介绍
canal mysql同步es
Canal是阿里开源产品之一，是用java开发的基于数据库增量日志解析，提供增量数据订阅&消费的中间件。目前，Canal主要支持了MySQL的binlog解析。

为何要解析binlog： binlog中含有许多我们需要的信息，基于这些信息，我们可以实现很多功能：

- 异构数据库同步
- 数据库事件触发实现分布式事务
- 数据检效与监控
- 等等

#### 基本原理：
MySQL主从同步原理：

master将改变记录到二进制日志(binary log)中（这些记录叫做二进制日志事件，binary log events，可以通过show binlog events进行查看）；
slave将master的binary log events拷贝到它的中继日志(relay log)；
slave重做中继日志中的事件，将改变反映它自己的数据。
Canal模拟binlog用的传输协议，把自己伪装成slave，抓取日志：

canal模拟mysql slave的交互协议，伪装自己为mysql slave，向mysql master发送dump协议
mysql master收到dump请求，开始推送binary log给slave(也就是canal)
canal解析binary log对象(原始为byte流)
#### 软件架构
springboot 集成springdatajpa es cannal


#### 安装教程

##### 配置测试数据库，开启binlog：
```
log-bin=mysql-bin #添加这一行就ok
binlog-format=ROW #选择row模式，虽然Canal支持各种模式，但是想用otter，必须用ROW模式
server_id=1 #配置mysql replaction需要定义，不能和canal的slaveId重复
```
#### 添加Canal用户(这里使用的root)：
```mysql
CREATE USER canal IDENTIFIED BY 'canal';  
GRANT SELECT, REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO 'canal'@'%';
FLUSH PRIVILEGES;
```
#### 下载安装canal.deployer-1.1.4.tar.gz
配置conf/example/instance.properties:
```
#################################################
## mysql serverId
## 这个id不能和目标源数据库的id一样
canal.instance.mysql.slaveId = 1234

# 数据库地址，binlog订阅开始点
canal.instance.master.address = 10.202.4.39:3308
canal.instance.master.journal.name = mysql-binlog.000005
canal.instance.master.position = 126596922
canal.instance.master.timestamp = 

# 配置备用源数据库
#canal.instance.standby.address = 
#canal.instance.standby.journal.name = 
#canal.instance.standby.position = 
#canal.instance.standby.timestamp = 

# username/password
canal.instance.dbUsername = canal
canal.instance.dbPassword = canal
canal.instance.defaultDatabaseName =
canal.instance.connectionCharset = UTF-8

# 订阅哪些表的binlog，支持正则表达式
canal.instance.filter.regex = .*\\..*
# 过滤掉的表的正则表达式
canal.instance.filter.black.regex =  

#################################################
```
#### 订阅起始点可自定义，查看当前binlog状态：
```mysql
show master status;
```
#### 配置conf/canal.properties:
```
#################################################
######### common argument ############# 
#################################################
canal.id= 1000001
canal.ip= 127.0.0.1
canal.port= 11111
# canal通过zk做负载均衡
canal.zkServers=
# flush data to zk
canal.zookeeper.flush.period = 1000
# flush meta cursor/parse position to file
canal.file.data.dir = ${canal.conf.dir}
canal.file.flush.period = 1000
## memory store RingBuffer size, should be Math.pow(2,n)
canal.instance.memory.buffer.size = 16384
## memory store RingBuffer used memory unit size , default 1kb
canal.instance.memory.buffer.memunit = 1024 
## meory store gets mode used MEMSIZE or ITEMSIZE
canal.instance.memory.batch.mode = MEMSIZE

## detecing config
canal.instance.detecting.enable = false
#canal.instance.detecting.sql = insert into retl.xdual values(1,now()) on duplicate key update x=now()
canal.instance.detecting.sql = select 1
canal.instance.detecting.interval.time = 3
canal.instance.detecting.retry.threshold = 3
canal.instance.detecting.heartbeatHaEnable = false

# support maximum transaction size, more than the size of the transaction will be cut into multiple transactions delivery
canal.instance.transaction.size =  1024
# mysql fallback connected to new master should fallback times
canal.instance.fallbackIntervalInSeconds = 60

# network config
canal.instance.network.receiveBufferSize = 16384
canal.instance.network.sendBufferSize = 16384
canal.instance.network.soTimeout = 30

# binlog filter config
canal.instance.filter.query.dcl = false
canal.instance.filter.query.dml = false
canal.instance.filter.query.ddl = false
canal.instance.filter.table.error = false

# binlog format/image check
canal.instance.binlog.format = ROW,STATEMENT,MIXED 
canal.instance.binlog.image = FULL,MINIMAL,NOBLOB

# binlog ddl isolation
canal.instance.get.ddl.isolation = false

#################################################
######### destinations ############# 
#################################################
canal.destinations= example
# conf root dir
canal.conf.dir = ../conf
# auto scan instance dir add/remove and start/stop instance
canal.auto.scan = true
canal.auto.scan.interval = 5

canal.instance.global.mode = spring 
canal.instance.global.lazy = false
#canal.instance.global.manager.address = 127.0.0.1:1099
#canal.instance.global.spring.xml = classpath:spring/memory-instance.xml
canal.instance.global.spring.xml = classpath:spring/file-instance.xml
#canal.instance.global.spring.xml = classpath:spring/default-instance.xml
```
#### 启动canal
```
./bin/startup.sh
```

#### 修改配置CanalClient
```
 
public CanalConnector getCanalConnector() {
        // cannal ip/cannal端口/canal.destinations/canal服务账号/canal服务密码
        canalConnector = CanalConnectors.newSingleConnector(new InetSocketAddress("127.0.0.1",
                11111), "example", "", "");
        canalConnector.connect();
        //指定filter,格式{database}.{table}
        canalConnector.subscribe(".*\\..*");
        //回滚寻找上次中断的为止
        canalConnector.rollback();
        return canalConnector;
    } 
    
```

#### 修改项目配置application.yml
```
##端口号配置
server:
  port: 8080

spring:
  ##druid数据源配置
  datasource:
    druid:
      # 数据库访问配置, 使用druid数据源
      db-type: com.alibaba.druid.pool.DruidDataSource
      driverClassName: com.mysql.cj.jdbc.Driver
      url: jdbc:mysql://127.0.0.1:3306/test?serverTimezone=UTC&useSSL=false&allowPublicKeyRetrieval=true
      username: root
      password: 123456
      # 连接池配置
      initial-size: 5
      min-idle: 5
      max-active: 20
      # 连接等待超时时间
      max-wait: 30000
      # 配置检测可以关闭的空闲连接间隔时间
      time-between-eviction-runs-millis: 60000
  ##Jpa配置
  jpa:
    hibernate:
      ddl-auto: update
    database-platform: org.hibernate.dialect.MySQL5InnoDBDialect
    show-sql: true
  ##ES配置
  elasticsearch:
    rest:
      uris: 127.0.0.1:9200
 ```
 #### 打开kibana 创建user索引
 ```
 PUT user
{
 "mappings": {
     "properties":{
       "id":{
         "type":"long"
       },
       "_email":{
         "type":"text"
       },
       "_gender":{
         "type":"text"
       },
       "_name":{
         "type":"text"
       }
     
   }
 }  
}
 ```
 #### 启动项目

 ```
 
  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.3.6.RELEASE)

2021-03-08 22:37:31.877  INFO 45020 --- [           main] com.duoduo.cannales.CannalEsApplication  : Starting CannalEsApplication on DESKTOP-DC11MCJ with PID 45020 (C:\Dev\Items\study\cannal-es\target\classes started by qian in C:\Dev\Items\study\cannal-es)
2021-03-08 22:37:31.881  INFO 45020 --- [           main] com.duoduo.cannales.CannalEsApplication  : No active profile set, falling back to default profiles: default
2021-03-08 22:37:32.295  INFO 45020 --- [           main] .s.d.r.c.RepositoryConfigurationDelegate : Bootstrapping Spring Data JPA repositories in DEFERRED mode.
2021-03-08 22:37:32.354  INFO 45020 --- [           main] .s.d.r.c.RepositoryConfigurationDelegate : Finished Spring Data repository scanning in 51ms. Found 1 JPA repository interfaces.
2021-03-08 22:37:32.583  INFO 45020 --- [           main] c.a.d.s.b.a.DruidDataSourceAutoConfigure : Init DruidDataSource
2021-03-08 22:37:32.792  INFO 45020 --- [           main] com.alibaba.druid.pool.DruidDataSource   : {dataSource-1} inited
2021-03-08 22:37:32.886  INFO 45020 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'applicationTaskExecutor'
2021-03-08 22:37:32.893  INFO 45020 --- [           main] o.s.s.c.ThreadPoolTaskScheduler          : Initializing ExecutorService 'taskScheduler'
2021-03-08 22:37:32.926  INFO 45020 --- [         task-1] o.hibernate.jpa.internal.util.LogHelper  : HHH000204: Processing PersistenceUnitInfo [name: default]
2021-03-08 22:37:32.975  INFO 45020 --- [         task-1] org.hibernate.Version                    : HHH000412: Hibernate ORM core version 5.4.23.Final
2021-03-08 22:37:33.095  INFO 45020 --- [         task-1] o.hibernate.annotations.common.Version   : HCANN000001: Hibernate Commons Annotations {5.1.2.Final}
2021-03-08 22:37:33.192  INFO 45020 --- [         task-1] org.hibernate.dialect.Dialect            : HHH000400: Using dialect: org.hibernate.dialect.MySQL5InnoDBDialect
2021-03-08 22:37:33.766  INFO 45020 --- [         task-1] o.h.e.t.j.p.i.JtaPlatformInitiator       : HHH000490: Using JtaPlatform implementation: [org.hibernate.engine.transaction.jta.platform.internal.NoJtaPlatform]
2021-03-08 22:37:33.776  INFO 45020 --- [           main] DeferredRepositoryInitializationListener : Triggering deferred initialization of Spring Data repositories…
2021-03-08 22:37:33.777  INFO 45020 --- [         task-1] j.LocalContainerEntityManagerFactoryBean : Initialized JPA EntityManagerFactory for persistence unit 'default'
2021-03-08 22:37:33.970  INFO 45020 --- [           main] DeferredRepositoryInitializationListener : Spring Data repositories initialized!
111
2021-03-08 22:37:33.979  INFO 45020 --- [           main] com.duoduo.cannales.CannalEsApplication  : Started CannalEsApplication in 2.542 seconds (JVM running for 3.495)
2021-03-08 22:37:33.992  INFO 45020 --- [   scheduling-1] c.d.cannales.config.CanalScheduling      : 更改前的数据:name:id,value:32
2021-03-08 22:37:33.992  INFO 45020 --- [   scheduling-1] c.d.cannales.config.CanalScheduling      : 更改前的数据:name:email,value:67890@67890
2021-03-08 22:37:33.992  INFO 45020 --- [   scheduling-1] c.d.cannales.config.CanalScheduling      : 更改前的数据:name:gender,value:MA
2021-03-08 22:37:33.992  INFO 45020 --- [   scheduling-1] c.d.cannales.config.CanalScheduling      : 更改前的数据:name:name,value:wen
2021-03-08 22:37:33.992  INFO 45020 --- [   scheduling-1] c.d.cannales.config.CanalScheduling      : 更改后的数据:name:id,value:32
2021-03-08 22:37:33.992  INFO 45020 --- [   scheduling-1] c.d.cannales.config.CanalScheduling      : 更改后的数据:name:email,value:67890@67890
2021-03-08 22:37:33.992  INFO 45020 --- [   scheduling-1] c.d.cannales.config.CanalScheduling      : 更改后的数据:name:gender,value:MAqw
2021-03-08 22:37:33.992  INFO 45020 --- [   scheduling-1] c.d.cannales.config.CanalScheduling      : 更改后的数据:name:name,value:wen
2021-03-08 22:37:33.992  INFO 45020 --- [   scheduling-1] c.d.cannales.config.CanalScheduling      : dataMap:{gender=MAqw, name=wen, id=32, email=67890@67890},database:test,table:user
Hibernate: select user0_.id as id1_0_0_, user0_.email as email2_0_0_, user0_.gender as gender3_0_0_, user0_.`name` as name4_0_0_ from user user0_ where user0_.id=?

 ```
 #### 打开kibana devToos查看
 ```
 GET user/_search
 
 {
  "took" : 1,
  "timed_out" : false,
  "_shards" : {
    "total" : 1,
    "successful" : 1,
    "skipped" : 0,
    "failed" : 0
  },
  "hits" : {
    "total" : {
      "value" : 1,
      "relation" : "eq"
    },
    "max_score" : 1.0,
    "hits" : [
      {
        "_index" : "user",
        "_type" : "_doc",
        "_id" : "32",
        "_score" : 1.0,
        "_source" : {
          "gender" : "MAqw",
          "name" : "wen",
          "id" : 32,
          "email" : "67890@67890"
        }
      }
    ]
  }
}
 ```
