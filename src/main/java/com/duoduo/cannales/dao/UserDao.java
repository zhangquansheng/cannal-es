package com.duoduo.cannales.dao;

import com.duoduo.cannales.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<User, Integer> {

}
