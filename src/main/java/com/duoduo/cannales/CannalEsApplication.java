package com.duoduo.cannales;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class CannalEsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CannalEsApplication.class, args);
    }

}
