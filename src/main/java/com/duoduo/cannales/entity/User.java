package com.duoduo.cannales.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author qian
 * @version 1.0
 * @date 2021/3/8 16:27
 */
@Entity
@Table(name = "user")
@Data
public class User {

    @Id
    private Integer id;

    @Column(name = "email")
    private String email;

    @Column(name = "gender")
    private String gender;

    @Column(name = "`name`")
    private String name;
}
